### Required

Node 12

### Installation

Install dependencies:

```bash
yarn install
```

Environment Variable:

```bash
cp .env.example .env

# open .env and modify the environment variables
```

### Commands

Running locally:

```bash
yarn dev
```

Running in production:

```bash
yarn start
```
