const express = require('express');
const coronaController = require('../../controllers/response.controller');

const router = express.Router();

router
  .route('/')
  .post(coronaController.createResponse)
  .get(coronaController.getResponses);


router
  .route('/:id')
  .post(coronaController.updateResponse)
  .get(coronaController.getResponse);

module.exports = router;
